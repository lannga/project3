
module.exports.checkFileEx = (file)=>{
  const {mimetype} = file
  if(mimetype.includes("image")){
    return "image"
  }
  else if(mimetype.includes("audio")){
    return "audio"
  }
  else
    return null
}