const { checkFileEx } = require("./configs/utils");
const model = require('./model')
const SUCCESS = 0
const ERROR = 1
const buildRes = async (func) =>{
  try {
    const data = await func()
    return ({
      code: SUCCESS,
      data
    })
  } catch (err) {
    return({
      code: ERROR,
      message: JSON.stringify(err)
    })
  }
}

module.exports.giveHeart= async (req, res)=>{
  console.log(req.body)
  const result = await buildRes(()=>model.giveHeart(req.body))
  res.json(result)
}

module.exports.listen= async (req, res)=>{
  const result = await buildRes(()=>model.listen(req.body))
  res.json(result)
}

module.exports.favorite= async (req, res)=>{
  const result = await buildRes(()=>model.favorite())
  res.json(result)
}

module.exports.chart= async (req, res)=>{
  const result = await buildRes(()=>model.chart())
  res.json(result)
}

module.exports.upload = async (req, res)=>{
  const {files, body} = req
  console.log(req.files)
  let input ={...body}
  files.forEach(file => {
    const path = file.path.replace(/\\/g,"/").replace("public/","")
    switch(checkFileEx(file)){      
      case "audio":
        input = Object.assign(input,{file: path})
        break;
      case "image":
        input = Object.assign(input,{cover: path})
        break;
    }
  });
  const result = await buildRes(()=>model.insert(input))
  res.json(result)
}

module.exports.search = async (req, res)=>{
  const result = await buildRes(()=>model.search(req.body))
  res.json(result)
}

module.exports.suggest = async (req, res)=>{
  const result = await buildRes(()=>model.suggest(req.body))
  res.json(result)
}

module.exports.getView=(req, res)=>{
  res.sendFile(__dirname+'/view/index.html')
}
