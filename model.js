const db = require('./configs/database');
const buildSearch = ({ searchText, limit , page = 1 }) => {
  let query = `select * from music where 1=1 `
  if (searchText) {
    query += ` and title like '%${searchText}%' or singer like '%${searchText}%' `
  }
  query += ` order by listen desc`
  if (limit){
    query += ` limit ${limit}`
  }
  return query
}

const cvtObj = data => JSON.parse(JSON.stringify(data))

const queryDB = async (sqlQuery) => {
  console.log(sqlQuery)
  const [rows, fields] = await db.query(sqlQuery)
  return cvtObj(rows)
}

module.exports.search = async ({searchText}) => {
  const data = await queryDB(buildSearch({searchText: searchText}))
  return data
}

module.exports.suggest = async ({searchText}) => {
  const data = await queryDB(buildSearch({searchText: searchText, limit: 5}))
  return data
}

module.exports.insert = async ({ title, singer, cover, file } = data) => {
  const query = `insert into music 
  (title, singer, cover, file) 
  values("${title}","${singer}","${cover}","${file}")  `
  const data = await queryDB(query)
  return data
}

module.exports.favorite = async () => {
  const data = await queryDB(`select * from music order by heart desc limit 10`)
  return data
}

module.exports.chart = async () => {
  const favChart = await queryDB(`select * from music order by heart desc limit 10`)
  const lisChart = await queryDB(`select * from music order by listen desc limit 10`)
  return {favChart, lisChart}
}

module.exports.giveHeart = async ({id}) => {
  const data = await queryDB(`update music set heart = heart +1 where id = ${id}`)
  return data
}

module.exports.listen = async ({id}) => {
  const data = await queryDB(`update music set listen = listen +1 where id = ${id}`)
  return data
}

