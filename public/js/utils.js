
const toHHMMSS = (iSeconds) => {
  const sec_num = parseInt(iSeconds, 10)
  const hours = Math.floor(sec_num / 3600)
  const minutes = Math.floor(sec_num / 60) % 60
  const seconds = sec_num % 60

  return [hours, minutes, seconds]
    .map(v => v < 10 ? "0" + v : v)
    .filter((v, i) => v !== "00" || i > 0)
    .join(":")
}

(function(window,undefined){
  '$:nomunge'; // Used by YUI compressor.
  var $ = window.jQuery || window.Cowboy || ( window.Cowboy = {} ),
    
    // Internal method reference.
    jq_throttle; 
  $.throttle = jq_throttle = function( delay, no_trailing, callback, debounce_mode ) {
    var timeout_id,
      last_exec = 0;

    if ( typeof no_trailing !== 'boolean' ) {
      debounce_mode = callback;
      callback = no_trailing;
      no_trailing = undefined;
    }

    function wrapper() {
      var that = this,
        elapsed = +new Date() - last_exec,
        args = arguments;

      function exec() {
        last_exec = +new Date();
        callback.apply( that, args );
      };
      function clear() {
        timeout_id = undefined;
      };
      
      if ( debounce_mode && !timeout_id ) {
        exec();
      }
      timeout_id && clearTimeout( timeout_id );
      
      if ( debounce_mode === undefined && elapsed > delay ) {
        exec();
        
      } else if ( no_trailing !== true ) {
        timeout_id = setTimeout( debounce_mode ? clear : exec, debounce_mode === undefined ? delay - elapsed : delay );
      }
    };

    if ( $.guid ) {
      wrapper.guid = callback.guid = callback.guid || $.guid++;
    }

    return wrapper;
  }; 
  
  $.debounce = function( delay, at_begin, callback ) {
    return callback === undefined
      ? jq_throttle( delay, at_begin, false )
      : jq_throttle( delay, callback, at_begin !== false );
  };
  
})(this);

const format = (n) => String(n).replace(/(.)(?=(\d{3})+$)/g,'$1,')



const BASE_URL = "http://localhost:3000/"
const buildUrl = key => BASE_URL+key
const url ={
  SEARCH: buildUrl("search"),
  UPLOAD: buildUrl("upload"),
  FAVORITE: buildUrl("favorite"),
  CHART: buildUrl("chart"),
  GIVE_HEART: buildUrl("give-heart"),
  LISTEN: buildUrl("listen"),
  SUGGEST: buildUrl("suggest")
}
const SUCCESS_CODE = 0
const postApi = async (url, data, isFormData = false)=>{
  console.log(data)
  let result = null
  const formData = isFormData ?{
    processData: !isFormData,
    contentType:  false
  }:{}
  await $.ajax({
    type: "POST",
    url: url,
    data: data,
    ...formData
  })
  .done(response=> result = response)
  .fail(message => console.log(message))
  if(result.code != SUCCESS_CODE){
    alert(result.message || "LỖi")
    return null
  }
  console.log(result)
  return result.data
}
