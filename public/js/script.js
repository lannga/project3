
let audio = new Audio();
audio.volume = 0.5
let currentSong = null
let currentIndexInPlaylist = 0
let playList = []
let chart = {
  favChart:[],
  lisChart:[]
}
let seaList = []
let isIncrease = false

let shuffle = true
let repeat = false

/// increase listen
const increaseListen = (id) => {
  postApi(url['LISTEN'], { id })
}

const onTimeUpdate = () => {
  const { currentTime, duration } = audio;
  const currentTimeString = toHHMMSS(currentTime)
  $('.time--current').text(currentTimeString)
  const percent = currentTime / duration * 100
  $('.progress-bar .fill').css({ 'width': percent + "%" })
  if (currentTime > 10&&currentTime<11 && !isIncrease) {
    isIncrease = true
    increaseListen(currentSong.id)
  }
  if(percent != 100){
    return
  }
  if(repeat){
    playMusic(true)
  }else{
    if(shuffle){
      let x = Math.floor(Math.random() * (playList.length-1));
      playMusic(true, playList[x], x)
    }else{
      playNext()
    }
  }
  
}

const onAudioLoaded = () => {
  audio.addEventListener("loadeddata", function () {
    $('.time--total').text(toHHMMSS(this.duration));
    audio.addEventListener('timeupdate', onTimeUpdate)
  });
}

const updateMusic = (song, index) => {
  const { title, singer, file, cover, listen } = song
  currentSong = song
  setCurrentIndex(parseInt(index))
  isIncrease = false
  audioPause()
  audio.src = file
  audio.load()
  onAudioLoaded()
  $('.song-info >span').empty()
  $('.song-info >span').append(`${format(listen) + " "}<i class="fas fa-headphones-alt"></i>`) 
  $('.song-info p').empty()
  $('.song-info p').append(`<span style="font-weight: bold">${title}</span> - <span>${singer}</span>`) 
  $('.album img').attr("src", cover)
}
const playMusic = (reset = false, song = currentSong, index = currentIndexInPlaylist) => {
  console.log("play")
  if (reset){
    updateMusic(song, index)
  }
  audio.play()
  $('.play').hide();
  $('.pause').show();
}
$('.play').click(() => playMusic());

const audioPause = function(){
  audio.pause();
  $('.play').show();
  $('.pause').hide();
}
$('.pause').click(audioPause);


const playPrev = function () {
  const index = currentIndexInPlaylist
  if (index > 0) {
    playMusic(true, playList[index - 1], index - 1)
  } else {
    playMusic(true, playList[playList.length-1], playList.length-1)
  }
}

const playNext = function () {
  const index = currentIndexInPlaylist
  if (index < playList.length && playList.length) {
    playMusic(true, playList[index + 1], index + 1)
  } else {
    playMusic(true, playList[0], 0)
  }
}
$('.next').click(playNext)
$('.previous').click(playPrev)

$('.repeat').click(function (){
  repeat = !repeat
  if(!repeat){
    $('.repeat div').hide()
  }
  else{
    $('.repeat div').show()
  }
})

$('.shuffle').click(function (){
  shuffle = !shuffle
  if(!shuffle){
    $(this).addClass("no-active")
  }
  else{
    $(this).removeClass("no-active")
  }
})

/// Volumn
const onVolumeChange = function () {
  const volume = $(this).val()
  audio.volume = volume / 100
}
$('.vol-control').change(onVolumeChange);

// ///       SEARCH
// const sampleSearch = [
//   { title: "Symphony", singer: "Clean Bandit ft. Zara Larsson" },
//   { title: "Aymphony", singer: "Clean Bandit ft. Zara Larsson" },
//   { title: "Bymphony", singer: "Clean Bandit ft. Zara Larsson" },
//   { title: "Cymphony", singer: "Clean Bandit ft. Zara Larsson" },
//   { title: "Dymphony", singer: "Clean Bandit ft. Zara Larsson" },
//   { title: "Eymphony", singer: "Clean Bandit ft. Zara Larsson" },
// ]
const searchInput = $('.search-form input')
const searchResult = $('.search-result')
const onSearch = async function () {
  const inputVal = $(this).val().toLowerCase();
  // const searchData = sampleSearch.filter(x => x.title.toLowerCase().includes(inputVal) && inputVal);
  let searchData =[]
  if(inputVal)
    searchData = await postApi(url['SUGGEST'], { searchText: inputVal }) || []
  searchResult.empty();
  searchData.forEach((val, i) => {
    const obj = encodeURIComponent(JSON.stringify(val))
    searchResult.append(`
      <div index = "${i}" obj="${obj}" class="item-suggest">
        <img src="${val.cover || "images/placeholder-music.png"}"/>
        <div>${val.title} - ${val.singer}</div>  
      </div>
    `)
  })
  searchResult.show();
  updateElement()
}
searchInput.focus(() => searchResult.show())
searchInput.on("input", $.debounce(100, onSearch))
// searchInput.blur((e) => {
//   console.log(e)
//   searchResult.hide();
//   searchResult.empty();
// })

$(window).click(function(e) {
  if($(e.target).parent().hasClass("item-suggest") || $(e.target).hasClass("item-suggest") ){
    return;
  }
  searchResult.hide();
  searchResult.empty()
})

///     LIST
const sampleList = [
  { id: 1, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "upload/image/CleanBAnditAlbum.jpg", file: "" },
  { id: 2, title: "Say So", singer: "Doja ft Nicky Minaj", cover: "upload/image/120620.jpg", file: "upload/audio/Say So - Doja Cat_ Nicki Minaj.mp3" },
  { id: 3, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 1000, listen: 500 },
  { id: 4, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 1000, listen: 50000 },
  { id: 5, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 1000, listen: 500 },
  { id: 6, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 1000, listen: 500 },
  { id: 7, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 1000, listen: 500 },
  { id: 8, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 1000, listen: 5000 },
  { id: 9, title: "Symphony", singer: "Clean Bandit ft. Zara Larsson", cover: "", file: "", heart: 10000, listen: 500 },
]
const onSearchSubmit = async function () {
  const result = await postApi(url['SEARCH'], {searchText: searchInput.val().toLowerCase()})
  const data = result || []
  seaList = data
  $('.result-search-info').empty()
  $('.result-search-info').append(`
  Tìm thấy: ${seaList.length} kết quả   
  <span class="play-playList" playList="SEA">    <i class="fas fa-play"></i></span>
  `)
  $('#a-songs').empty()
  data.forEach((val, i) => {
    const obj = encodeURIComponent(JSON.stringify(val))
    $('#a-songs').append(`
      <li index = "${i}" obj="${obj}" class="list-item">
        <img src="${val.cover || "images/placeholder-music.png"}"/>    
        <div style='flex:1'>            
          <div class="item-title">${val.title}</div>
          <div class="item-singer">${val.singer}</div>   
        </div>
        <div class="item-listen">${format(val.listen) + " "}<i class="fas fa-headphones-alt"></i></div>  
        <div class="item-heart give-heart" song-id="${val.id}"><i class="fas fa-heart"></i></div>            
      </li>
    `)
  })
  updateElement()
}
$('.search-btn').click(onSearchSubmit)

///     FORM
const onCloseForm = function (e) {
  if (e.target != this)
    return
  $(this).hide()
}
$('#form').click(onCloseForm)
$('#upload').click(() => $('#form').show())


/// get favorite
const onChartSong = async () => {
  const result = await postApi(url.CHART, {})
  chart = result || chart
  renderChartSongs(chart.favChart, $('#f-songs'), true)
  renderChartSongs(chart.lisChart, $('#l-songs'))
}

const renderChartSongs = (list, ele, isFav)=>{
  ele.empty()
  list.forEach((val, i) => {
    const obj = encodeURIComponent(JSON.stringify(val))
    ele.append(`
      <li index = "${i}" obj="${obj}" class="list-item-f" playList="${isFav ? "FAV":"LIS"}">
        <div style="font-size: 1em; width: 20px; text-align: right">${i + 1}</div>
        <img src="${val.cover || "images/placeholder-music.png"}"/>
        <div style='flex:1'>        
          <div class="item-title">${val.title}</div>
          <div class="item-singer">${val.singer}</div>          
          <div class="item-heart sm">${format(isFav ? val.heart: val.listen)} <i class="${isFav ? "far fa-heart": "fas fa-headphones-alt"}"></i></div>          
        </div>           
      </li>
    `)
  })
  updateElement()
}

const renderCurrentPlayList = ()=>{
  $('#current-playlist').empty()
  playList.forEach((val, i) => {
    const obj = encodeURIComponent(JSON.stringify(val))
    
    $('#current-playlist').append(`
      <li index = "${i}" obj="${obj}" class="list-item-f">
        <img src="${val.cover || "images/placeholder-music.png"}"/>
        <div style="flex:1">
          <div class="item-title">${val.title}</div>
          <div class="item-singer">${val.singer}</div>
        </div>
        ${ i == currentIndexInPlaylist ?
        `<div>
          <img src="./images/signal.gif">
        </div>
        `
        :""
        }
      </li>
    `)
  })
  updateElement()
}

$('.progress-bar').click(function(e){
  const percent = e.offsetX/this.clientWidth
  audio.currentTime = audio.duration*percent
})

const updateElement = ()=>{
  $('.give-heart').unbind()
  $('.give-heart').click(giveHeart)
  $(`#a-songs > li > img
  , #a-songs > li > div:nth-child(2)`).unbind()
  $(`#a-songs > li > img
  , #a-songs > li > div:nth-child(2)`).click(onItemSearchChose)
  $(`.list-item-f`).unbind()
  $(`.list-item-f`).click(onItemFavChose)
  $(`.item-suggest`).unbind()
  $(`.item-suggest`).click(onItemSuggestChose)
}

const onItemSuggestChose = function(){
  console.log("suggest")
  onItemChose($(this))
  searchResult.hide();
  onSearchSubmit()
  setPlayList(seaList)
}

const onItemSearchChose = function(){
  console.log("search")
  setPlayList(seaList)
  onItemChose($(this).parent())
}

const onItemFavChose = function(){
  setPlayList($(this).attr("playList") == "FAV" ? chart.favChart: chart.lisChart)
  onItemChose($(this))
}

const onItemChose = function (item) {
  let song = item.attr("obj")
  let index = item.attr("index")
  song = JSON.parse(decodeURIComponent(song))
  playMusic(true, song, index)
}


/// give Heart
const giveHeart = function () {
  const target = $(this)
  target.bind("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(){
    $(this).removeClass("wiggle");
  }).addClass("wiggle")
  const id = target.hasClass("on-play") ? currentSong.id : target.attr("song-id")
  postApi(url['GIVE_HEART'], { id })
}

// setPlayList
const setPlayList = (pList)=>{
  playList = pList
}

const setCurrentIndex = (index) =>{
  currentIndexInPlaylist = index
  renderCurrentPlayList()
}


//////
$('.form button').click(function () {
  const title = $('.form input[name="title"]').val()
  const singer = $('.form input[name="singer"]').val()
  if (!title) {
    return alert("Chưa nhập tên bài hát")
  }
  if (!singer) {
    return alert("Chưa nhập tên nghệ sỹ")
  }
  const form = new FormData();
  form.append('title', title)
  form.append('singer', singer)
  $.each($(".form input[type='file']"), function (i, file) {
    if (!file.accept.includes('image') && i == 0) {
      return alert("Chưa chọn file cover")
    }
    if (!file.accept.includes('audio') && i == 1) {
      return alert("Chưa chọn file audio")
    }
    if (file.files.length !== 0) {
      form.append('file', file.files[0]);
    }
  });
  postApi(url.UPLOAD, form, true).then(respone => {    
    if(respone != null){
      alert("Thành công")
      $('.form input').val("")
    }
  })
});
onSearchSubmit()
onChartSong()






