const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer')
const cors = require('cors')
const app = express();
const controller = require('./controller')
app.use( bodyParser.json({limit: '50mb'}) );
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit:50000
}));
app.use(cors())
app.use(express.static('public'));

const { checkFileEx } = require('./configs/utils');
let upload = multer({
  storage: multer.diskStorage({
    destination: (req, file, callback) => {
      let path = "./public/upload/"+checkFileEx(file)
      callback(null, path);
    },
    onFileUploadStart: function(file, req, res){
      if(!checkFileEx(file)) {
        return false;
      }
    }
  })
});

app.get('/', function (req, res) {
   controller.getView(req, res) 
});

app.post('/upload', upload.array('file', 2),function (req, res) {
  controller.upload(req, res)
});

app.post('/search', function (req, res) {
  controller.search(req, res)
});

app.post('/suggest', function (req, res) {
  controller.suggest(req, res)
});

app.post('/favorite', function (req, res) {
  controller.favorite(req, res)
});

app.post('/chart', function (req, res) {
  controller.chart(req, res)
});

app.post('/give-heart', function (req, res) {
  controller.giveHeart(req, res)
});

app.post('/listen', function (req, res) {
  controller.listen(req, res)
});
var server = app.listen(3000, function () {
  var host = server.address().address
  var port = server.address().port
});

